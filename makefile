BUILD: clean
	smu buttons.md > buttons.partial
	smu nav.md > nav.partial
	mkdir ../www
	zod . ../www

.PHONY: clean
clean:
	rm -rf ../www
	rm -f buttons.partial
	rm -f nav.partial
