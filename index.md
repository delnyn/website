# /delnyn/

I'm delnyn, an engineering student.

I enjoy:

* programming (low level stuff in C)
* maths
* manga and anime:<ul>
* serial experiments lain
* neon genesis evangelion (you might have noticed)
* vagabond 
* berserk
* GTO
* video games (I should play more tho, espescially nier, souls*like, og zeldas etc)</ul>
* music:<ul>
* radiohead
* david bowie
* gorillaz
* glass animals
* slipknot
* gojira
* panchiko
* and others</ul> 

I might post stuff here about what I make.
 
## Programming

My main project at the moment is an [operating system](https://codeberg.org/delnyn/os)
. It's in early stage.

In the future, I'd like to look into:

* complilers/programming languages
* virtual machines
* custom cpu archs
* clean UI description language
 
I host my projects on [Codeberg](https://codeberg.org/delnyn).

## Contact me

@delnyn on [Discord](https://discord.com/app) (I don't recommend using Discord though).

[delnyn on Codeberg](https://codeberg.org/delnyn).

![lain my beloved](assets/sel.gif)
