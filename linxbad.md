# Reasons people don't use Linux

It's not (only) GUI. I try here to summarise the biggest reasons 'normal'
 people don't use Linux

## Hardware compatibility... is hard

Here, I'm not only talking about NVIDIA. If you install linux on a computer,
 (and especially laptops) you have an about 50% chance that something won't
 work (correctly) by default. WiFi, bluetooth, sound card, graphics card,
 ACPI, you name it.

It can be fixed, yes, but 'normal' people don't want to - and likely won't -
 bother. 

### Solution

A GUI software that lets you install drivers, update them, tells you if they
 are the best you can get, where to download them, tells you which software
 supports these drivers and can configure them.

So normal people can do this quickly and without using a terminal.

## The big conspiracy of OSes

No mainstream brand ships computers with Linux (exceptions do exist though).
 Why is that? Basically, Microsoft has contracts with computer manufacturers,
 so they 'have' to ship Windows.

### Solution

Tell a friend. But DON'T TRY TOO HARD. Tell people:

* you use Linux
* it works
* it's fast
* it can save old computers
* it can be customized in cool ways
* it's very powerful for programming
* it's LIBRE and ETHICAL

That's it. If you try too hard to convert them, they will find it annoying, and
 this won't serve Linux.

Help people. If you want your grandma's 10 year old computer to be fast, up
 to date and secure, install a good distro for her (most importantly with her
 consent). And a good one, not Ubuntu, try Mageia Gnome or Pop_OS!, or Mint or
 whatever.

## Linux has nothing more to offer

Other than extended lifetime.

People who just want to browse the web, read mail, watch Netflix, listen to
 Spotify, read PDFs, and edit documents and spreadsheets have no reason to go
 through the hassle of switching away from Windows. 

Hell, some people are even capable of buying new hardware because it just
 'can't keep up'.

That's why 'normal people' Linux distros mustn't get in the way.

![recall](assets/copland.gif)
